package io.newsful.reddit

import groovy.json.JsonSlurper
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class RedditMappersTest extends Specification {

    @Shared
    def jsonSlurper = new JsonSlurper()

    @Unroll
    def "sanitizing a listing that doesn't have a 'kind' field of 'Listing' fails"() {
        when:
        RedditMappers.sanitizeListing(listing as Map)

        then:
        thrown exception

        where:
        listing                                                        || exception
        jsonSlurper.parse("{}".toCharArray())                          || IllegalArgumentException
        jsonSlurper.parse("""{"kind": "NotAListing"}""".toCharArray()) || IllegalArgumentException
    }

    def "sanitizing a listing yields the content of its 'data' element"() {
        given:
        Map aListing = jsonSlurper.parse(LISTING.toCharArray()) as Map

        when:
        Map sanitaziedListing = RedditMappers.sanitizeListing(aListing)

        then:
        def children = sanitaziedListing["children"] as List
        children.size() == 1
        children[0]["data"]["key"] == "value"
        children[0]["kind"] == "t3"
    }

    @Unroll
    def "sanitizing a thread link that doesn't have a 'kind' field of 't3' fails"() {
        when:
        RedditMappers.sanitizeThreadLink(threadLink as Map)

        then:
        thrown exception

        where:
        threadLink                                                || exception
        jsonSlurper.parse("{}".toCharArray())                     || IllegalArgumentException
        jsonSlurper.parse("""{"kind": "NotAT3"}""".toCharArray()) || IllegalArgumentException
    }

    def "sanitizing a thread link yields the content of its 'data' element"() {
        given:
        Map aThreadLink = jsonSlurper.parse(THREAD_LINK.toCharArray()) as Map

        when:
        Map sanitizedThreadLink = RedditMappers.sanitizeThreadLink(aThreadLink)

        then:
        sanitizedThreadLink["id"] == "1"
    }

    @Unroll
    def "sanitizing a comment that doesn't have a 'kind' field of 't1' fails"() {
        when:
        RedditMappers.sanitizeComment(comment as Map)

        then:
        thrown exception

        where:
        comment                                                   || exception
        jsonSlurper.parse("{}".toCharArray())                     || IllegalArgumentException
        jsonSlurper.parse("""{"kind": "NotAT1"}""".toCharArray()) || IllegalArgumentException
    }

    def "sanitizing a comment yields the content of its 'data' element"() {
        given:
        Map aComment = jsonSlurper.parse(COMMENT.toCharArray()) as Map

        when:
        Map sanitizedThreadLink = RedditMappers.sanitizeComment(aComment)

        then:
        sanitizedThreadLink["id"] == "2"
    }

    def "mapping a thread link returns a Map with only the fields we want"() {
        given:
        Map aThread = jsonSlurper.parse(THREAD_LINK.toCharArray()) as Map

        when:
        Map mappedThread = RedditMappers.mapThreadLink(aThread as Map)

        then:
        mappedThread["id"] == "1"
        mappedThread["title"] == "title1"
        mappedThread["selftext"] == "text1"
        mappedThread["author"] == "author1"
        mappedThread["date"] == 1519315548
        mappedThread["ups"] == 1
        mappedThread["downs"] == 1
        mappedThread["link"] == "https://url1"
    }


    def "mapping a comment returns a Map with only the fields we want"() {
        given:
        Map aComment = jsonSlurper.parse(COMMENT.toCharArray()) as Map

        when:
        Map mappedComment = RedditMappers.mapComment(aComment as Map)

        then:
        mappedComment["id"] == "2"
        mappedComment["body"] == "body2"
        mappedComment["author"] == "author2"
        mappedComment["date"] == 1517073505
        mappedComment["ups"] == 2
        mappedComment["downs"] == 2

        and:
        def replies = mappedComment["replies"] as List
        replies.size() == 1
        replies[0]["id"] == "3"
        replies[0]["body"] == "body3"
        replies[0]["author"] == "author3"
        replies[0]["date"] == 1517320775
        replies[0]["ups"] == 3
        replies[0]["downs"] == 3
    }

    public static final String LISTING = """
{
    "data": {
        "children": [
            {
                "data": {
                    "key": "value",
                },
                "kind": "t3"
            }
        ]
    },
    "kind": "Listing"
}
"""

    public static final String THREAD_LINK = """
{
    "data": {
        "author": "author1",
        "created_utc": 1519315548.0,
        "id": "1",
        "selftext": "text1",
        "title": "title1",
        "ups": 1,
        "downs": 1,
        "url": "https://url1",
    },
    "kind": "t3"
}
"""

    static String COMMENT = """
{
    "data": {
        "author": "author2",
        "body": "body2",
        "created_utc": 1517073505.0,
        "downs": 2,
        "id": "2",
        "replies": {
            "data": {
                "children": [
                    {
                        "data": {
                            "author": "author3",
                            "body": "body3",
                            "created_utc": 1517320775.0,
                            "downs": 3,
                            "id": "3",
                            "replies": "",
                            "ups": 3
                        },
                        "kind": "t1"
                    }
                ]
            },
            "kind": "Listing"
        },
        "ups": 2,
    },
    "kind": "t1"
}
"""

}
