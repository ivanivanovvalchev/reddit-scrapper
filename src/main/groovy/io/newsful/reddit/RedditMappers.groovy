package io.newsful.reddit


class RedditMappers {

    private static final DATE_REGEX = /(\d+)(\.\d*)*/

    static Map sanitizeListing(Map listing) {
        if (!"listing".equalsIgnoreCase(listing["kind"] as String)) {
            throw new IllegalArgumentException("not a listing: $listing")
        }
        return sanitizeRedditObject(listing)
    }

    private static Map sanitizeRedditObject(Map redditObject) {
        if (redditObject["data"] == null || !redditObject instanceof Map) {
            throw new IllegalArgumentException("reddit object does not have data: $redditObject")
        }
        return redditObject["data"] as Map
    }

    private static Map sanitizeThreadLink(Map threadLink) {
        if (!"t3".equalsIgnoreCase(threadLink["kind"] as String)) {
            throw new IllegalArgumentException("not a thread link: $threadLink")
        }
        return sanitizeRedditObject(threadLink)
    }

    private static Map sanitizeComment(Map comment) {
        if (!"t1".equalsIgnoreCase(comment["kind"] as String)) {
            throw new IllegalArgumentException("not a comment: $comment")
        }
        return sanitizeRedditObject(comment)
    }

    static Map mapThreadLink(Map threadLink) {
        def sanitizedThreadLink = sanitizeThreadLink(threadLink)
        return ["id"      : sanitizedThreadLink["id"],
                "title"   : sanitizedThreadLink["title"],
                "selftext": sanitizedThreadLink["selftext"],
                "author"  : sanitizedThreadLink["author"],
                "date"    : (sanitizedThreadLink["created_utc"] =~ DATE_REGEX)[0][1] as Long,
                "ups"     : sanitizedThreadLink["ups"],
                "downs"   : sanitizedThreadLink["downs"],
                "link"    : sanitizedThreadLink["url"]]
    }

    static Map mapComment(Map comment) {
        def sanitizedComment = sanitizeComment(comment)
        def replies = sanitizedComment["replies"]
        def mappedReplies = (replies == null || (replies as String).empty) ? []
                : (sanitizeListing(replies as Map)["children"] as List)
                .collect { mapComment(it as Map) }

        return ["id"     : sanitizedComment["id"],
                "body"   : sanitizedComment["body"],
                "author" : sanitizedComment["author"],
                "date"   : (sanitizedComment["created_utc"] =~ DATE_REGEX)[0][1] as Long,
                "ups"    : sanitizedComment["ups"],
                "downs"  : sanitizedComment["downs"],
                "replies": mappedReplies]
    }
}
