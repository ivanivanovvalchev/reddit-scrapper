package io.newsful.reddit

import com.mongodb.MongoClient
import com.mongodb.client.MongoCollection
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import groovy.json.JsonSlurper
import org.bson.Document

import java.util.function.Consumer

import static RedditMappers.*

class Main {
    final static def httpClient = new OkHttpClient()
    final static def jsonSlurper = new JsonSlurper()
    final static def userAgent = UUID.randomUUID().toString()
    final static def mongo = new MongoClient() // expected mongo to be running on localhost

    static void main(String[] args) {
        def subredditName = "stock"

        def redditDb = mongo.getDatabase("reddit")
        def col = redditDb.getCollection(subredditName)

        def topics = fetchComments(subredditName, fetchSubreddit(subredditName))
        println topics[3]

        insertInMongo(col, topics)
        col.find().forEach({ println it } as Consumer<Document>)
    }

    static fetchSubreddit(subredditName) {
        def response = httpClient.newCall(new Request.Builder()
                .url("https://www.reddit.com/r/$subredditName/new.json")
                .addHeader("user-agent", userAgent)
                .addHeader("Accept", "application/json")
                .build())
                .execute()

        def subredditListing = sanitizeListing(jsonSlurper.parse(response.body().byteStream()) as Map)
        return (subredditListing["children"] as List).collect {
            mapThreadLink(it as Map)
        }
    }

    static def fetchComments(subredditName, threadLinks) {
        return (threadLinks as List).collect { t ->
            def response = httpClient.newCall(new Request.Builder()
                    .url("https://www.reddit.com/r/$subredditName/comments/${t["id"]}/new.json")
                    .addHeader("user-agent", userAgent)
                    .addHeader("Accept", "application/json")
                    .build())
                    .execute()

            // the response is array of which the first element is the thread link
            // and the second is a listing with the comments
            def jsonResponse = jsonSlurper.parse(response.body().byteStream()) as List
            def commentsListing = sanitizeListing(jsonResponse[1] as Map)

            t["comments"] = (commentsListing["children"] as List).collect {
                mapComment(it as Map)
            }
            return t
        }
    }

    static def insertInMongo(MongoCollection<Document> col, def docs) {
        col.insertMany((docs as List).collect { new Document(it) })
    }
}
